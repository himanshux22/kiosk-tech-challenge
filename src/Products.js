import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import Consts from './utils/Const';
import {FlatGrid} from 'react-native-super-grid';

let {CurrDeviceWidth} = Consts;

export default class Products extends Component {
  state = {
    productList: [],
  };
  componentDidMount = async () => {
    let ProductList = this.props.productList;
    if (ProductList) {
      this.setState({productList: ProductList});
    }
  };

  RenderProducts = item => {
    return (
      <View style={styles.renderView}>
        <View>
          <Text style={styles.heading}>{item.name}</Text>

          <Text>
            ${item.price + '  '}
            {0 + '  '}cal
          </Text>
        </View>
        <Image
          style={styles.Img}
          source={{
            uri: `https://via.placeholder.com/300.png?text=${this.props.tabLabel}`,
          }}
        />
      </View>
    );
  };
  render() {
    return (
      <View style={styles.mainView}>
        {this.state.productList.length != 0 ? (
          <FlatGrid
            itemDimension={CurrDeviceWidth / 4}
            items={this.state.productList}
            renderItem={({item}) => this.RenderProducts(item)}
            style={{flex: 1}}
          />
        ) : (
          <Text style={styles.noProductText}>
            No Products on selected Category
          </Text>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainView: {
    width: CurrDeviceWidth / 1.4,
    flex: 1,
  },
  noProductText: {fontSize: 22, margin: 10},
  heading: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#33294e',
    width: CurrDeviceWidth / 5,
  },
  Img: {
    width: 120,
    height: 120,
    resizeMode: 'cover',
    borderColor: '#ddd',
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
  },
  renderView: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 5,
    borderColor: '#ddd',
    borderWidth: 2,
    padding: 6,
  },
});
