import {Dimensions} from 'react-native';

module.exports = {
  // Change the api port if your server is running in different port

  apiUrl: 'http://localhost:3001',
  CurrDeviceWidth: Dimensions.get('window').width,
  CurrDeviceHeight: Dimensions.get('window').height,
};
