import axios from 'axios';

import Consts from './Const';

let {apiUrl} = Consts;
//getapi call
export function callGetService(uri) {
  return new Promise((resolve, reject) => {
    axios
      .get(apiUrl + uri)
      .then(response => {
        resolve(response);
      })
      .catch(error => {
        resolve(error);
      });
  });
}
