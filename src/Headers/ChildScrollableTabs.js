import React, {Component} from 'react';
import Products from '../Products';

var ScrollableTabView = require('react-native-scrollable-tab-view');

export default class ChildScrollableTabs extends Component {
  state = {
    categories: [],
  };
  componentDidMount = async () => {
    let Categories = this.props.categories;
    if (Categories) {
      this.setState({categories: Categories});
    }
  };
  render() {
    return (
      <ScrollableTabView
        tabBarBackgroundColor="#351d5e"
        tabBarActiveTextColor="white"
        tabBarUnderlineStyle={{backgroundColor: 'white'}}
        tabBarInactiveTextColor="#c6bcd6">
        {this.state.categories.length != 0 ? (
          this.state.categories.map(item => {
            return (
              <Products
                key={item.id}
                tabLabel={item.name}
                productList={item.items}
              />
            );
          })
        ) : (
          <Products tabLabel="Server not Running" />
        )}
      </ScrollableTabView>
    );
  }
}
