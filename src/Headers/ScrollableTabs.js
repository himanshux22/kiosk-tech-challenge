import React, {Component} from 'react';

import ChildScrollableTabs from '../Headers/ChildScrollableTabs';
import {callGetService} from '../utils/Services';

import ScrollableTabView, {
  DefaultTabBar,
} from 'react-native-scrollable-tab-view';

export default class ScrollableTabs extends Component {
  state = {
    MenuGroups: [],
  };
  componentDidMount = async () => {
    await callGetService('/api/menu')
      .then(res => {
        console.log(res.data);
        if (res.status == '200') {
          this.setState({MenuGroups: res.data.data.MenuGroups});
        }
      })
      .catch(error => {
        console.log(error);
      });
  };
  render() {
    return (
      <ScrollableTabView
        tabBarBackgroundColor="#3e2572"
        tabBarActiveTextColor="white"
        tabBarUnderlineStyle={{backgroundColor: 'white'}}
        tabBarInactiveTextColor="#c6bcd6">
        {this.state.MenuGroups.length != 0 ? (
          this.state.MenuGroups.map(item => {
            return (
              <ChildScrollableTabs
                key={item.id}
                tabLabel={item.name}
                categories={item.categories}
              />
            );
          })
        ) : (
          <ChildScrollableTabs tabLabel="Default Heading" />
        )}
      </ScrollableTabView>
    );
  }
}
